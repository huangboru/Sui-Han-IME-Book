# 岁寒输入法 从入门到入坑

本书将向您详细介绍岁寒输入法的核心理念和规则，各项功能的具体使用方法等，将被作为日后岁寒输入法最权威的官方使用手册。

本书会紧跟岁寒输入法的迭代脚步进行内容更新。

一切关于岁寒输入法的问题，您都可以尝试查阅本书寻找答案。如无果，请与我联系(邮箱：443570993@qq.com)，我会耐心地回答您任何关于岁寒输入法的问题，并根据情况更新本书内容。期待您给我以启发。

同时，也期待您向本书的[码云仓库](https://gitee.com/huangboru/Sui-Han-IME-Book)提交issue，以使本书更为完善、丰富和详实。

